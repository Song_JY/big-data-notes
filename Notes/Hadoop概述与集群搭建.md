## Hadoop概述与集群搭建

### Hadoop概述

#### Hadoop组成

![image-20230802101925445](Hadoop概述与集群搭建.assets/image-20230802101925445.png)



#### 大数据技术框架

![image-20230802101839340](Hadoop概述与集群搭建.assets/image-20230802101839340.png)





#### HDFS

Hadoop Distributed File System，简称HDFS，是一个分布式文件系统.

#### YARN

Yet Another Resource Negotiator简称YARN ，另一种资源协调者，是Hadoop的资源管理器。

#### MapReduce

MapReduce将计算过程分为两个阶段：Map和Reduce

1）Map阶段并行处理输入数据

2）Reduce阶段对Map结果进行汇总



### Hadoop集群搭建



#### 集群角色规划

![image-20230730130236425](Hadoop概述与集群搭建.assets/image-20230730130236425.png)

|      | hadoop102              | hadoop103                        | hadoop104                       |
| ---- | ---------------------- | -------------------------------- | ------------------------------- |
| HDFS | NameNode<br />DataNode | <br />DataNode                   | SecondaryNameNode<br />DataNode |
| YARN | <br />NodeManager      | ResourceManager<br />NodeManager | <br />NodeManager               |





#### 集群启动停止方式总结

​	（1）整体启动/停止HDFS

start-dfs.sh/stop-dfs.sh

​	（2）整体启动/停止YARN

start-yarn.sh/stop-yarn.sh

**各个服务组件逐一启动停止**

​	（1）分别启动/停止HDFS组件

hdfs --daemon start/stop namenode/datanode/secondarynamenode

​	（2）启动/停止YARN

yarn --daemon start/stop  resourcemanager/nodemanager



**集群操作**

start|stop-dfs.sh 在master上使用,否则只会停掉该台机器的datanode(namenode)

start|stop-yarn.sh(会停掉ResourceManager与NodeManager)在配置ResourceManager的机器上使用,否则只会停止该台机器上的NodeManager

start|stop-all.sh(不建议对集群使用)



### 常见问题总结：

**HDFS页面删除文件时提示dr.who**

配置core-site.xml文件

```
    <!-- 配置HDFS网页登录使用的静态用户为atguigu -->
    <property>
        <name>hadoop.http.staticuser.user</name>
        <value>atguigu</value>
    </property>
```

