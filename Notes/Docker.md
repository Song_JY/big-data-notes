### Docker基本使用

**拉取镜像**

**创建容器**

docker run -it --name myhadoop -p 80:80 ubuntu

- docker run :创建并运行一个容器

- -i: 以交互模式运行容器，通常与 -t 同时使用；

- -t: 为容器重新分配一个伪输入终端，通常与 -i 同时使用；

- --name:给容器起一个名字，比如叫做：myhadoop

- -p：将宿主机端口与容器端口映射，冒号左侧是宿主机端口，右侧是docker容器端口

- ubuntu：容器名称
- -h：指定容器的hostname



**删除镜像**

docker rmi [image]

**启动容器**

docker start ID或容器名

**进入容器**

docker exec -it 容器id /bin/bash

docker attach 容器id

**关闭容器**

docker kill

docker stop $(docker ps -a -q) 停止所有容器

**删除容器**

docker  rm $(docker ps -a -q) 删除所有容器

**重启容器**

docker restart ID



杀死所有正在运行的容器: 
 docker kill `docker ps -aq`
删除所有停止的容器: 
 docker rm `docker ps -aq`
强制删除所有的容器 ：
 docker rm -f `docker ps -aq`