# Hive

## 数据仓库基础理论

**主要用于分析数据，而不生产数据。**

### 四个特征：

1. 面向主题

   - 对企业中某一宏观分析领域所涉及的分析对象。

   - 操作性数据（传统数据）对数据的划分并不适用于决策分析，而基于主题的数据则不同，它们被划分为各自独立的领域，每个领域有各自的逻辑内涵但互不交叉，在抽象层次上对数据进行完整、一直和准确的描述。

2. 集成性

   - 数据通常会分布在多个操作性系统中，彼此分散、独立、异构。

   - (ETL)因此在数据进入数据仓库之前必然要经过统一于综合，对数据进行抽取、清理、转换和汇总，这一步是数据仓库建设中最关键、复杂的一步，要完成的工作有：
     - 统一数据源，如字段的同名异义、异名同义、单位不统一、字长不一致等
     - 进行数据的综合和计算。

3. 非易失性

   - 数据仓库是数据分析的平台。
   - 不需要每一笔业务都更新数据仓库，只需要保存过去的数据。
   - 数据仓库一般有大量的查询操作，修改和删除很少。

4. 时变性

   - 数据仓库的数据随时间的变化远长于操作性数据的数据时限
   - 数据仓库中的数据是历史数据，操作性系统存放的是当前数据。
   - 数仓中的数据是按照时间顺序追加的，都带有时间属性。

### OLTP和OLAP

概念：

联机事务处理OLTP

联机分析处理OLAP

**OLTP**

操作性处理，主要目标是做数据处理，增删改查。

数据的安全性，响应时间，完整性和并发支持的用户数等问题。

传统的关系型数据库系统作为数据管理的主要手段，主要用于操作性处理。

**OLAP**

分析性处理，主要目标是数据分析

**对比**

|          | OLTP                         | OLAP                                             |
| -------- | ---------------------------- | ------------------------------------------------ |
| 数据源   | 仅包含当前运行日常业务的数据 | 整合多个来源的数据，包括OLTP和外部来源           |
| 目的     | 面向应用、面向业务、支撑事务 | 面向主题、面向分析、支撑分析决策                 |
| 焦点     | 当下                         | 主要面向过去，实时数仓除外                       |
| 任务     | 读写操作                     | 大量读很少写                                     |
| 响应时间 | 毫秒                         | 秒、分钟、小时、天<br />取决于数据量和查询复杂性 |
| 数据量   | 小数据，MB,GB                | TB,PB                                            |



### 数据集市

数据仓库是面向整个集团组织的数据，数据集市是面向单个部门使用的。

可以认为数据集市是数据仓库的子集。数据集市通常只涉及一个主题领域。



### 数据仓库分层架构

最基础的分层思想：操作型数据层ODS、数据仓库层DW、数据应用层DA。

数据应用层：面向最终用户，面向业务定制提供给产品和数据分析使用的数据。

为什么分层？

- 清晰数据结构

- 数据追踪

  精准定位来源

- 减少重复开发

- 将复杂问题简单化

  每一层只处理单一步骤，便于维护

- 屏蔽原始数据的异常

  屏蔽业务的影响，不必改一次业务就重新接入数据



### ETL和ELT

**ETL（抽取、转换、加载）**

首先从数据源池中提取数据，这些数据源通常是事务型数据库。数据保存在临时暂存数据库中（ODS）。然后执行转换操作，将数据结构化并转换为适合目标数据仓库系统的形式。然后将结构化数据加载到仓库中，以备分析。

**ELT**

提取后立即加载。没有专门的临时数据库。数据在数据仓库系统中进行转换，以便与商业化智能工具（BI工具）一起使用。

 

## Hive概述

Hive能将数据文件映射成一张表

映射：文件和表的对应关系

Hive软件本身的功能：

SQL语法解析编译成MapReduce



HIve使用HDFS存储数据，使用MapReduce计算



### Hive架构 、组件

**用户接口**

**元数据存储**

通常存储在关系型数据库中

**Driver驱动程序**

解析SQL语句，生产查询计划存储在HDFS中。

**执行引擎**

Hive本身不直接处理数据文件，而是通过执行引擎处理。当下Hive支持MapReduce,Tez,Spark 三种执行引擎。

**架构图：**

![image-20230812172558209](Hive.assets/image-20230812172558209.png)



### Hive数据模型

#### 数据库

Hive作为一个数据仓库，在结构上向传统数据库看起，也分数据库，每个数据库下有各自的表。默认数据库default.

Hive的数据存储在HDFS上，默认有一个根目录，在hive-site.xml中

#### 表

Hive表与关系数据库的表相同，表对应的数据存储在HDFS中，而表相关的元数据存储在RDBMS中。

#### 分区

- 优化手段，根据分区列的值将表划分为不同的分区，方便查询。

- 在存储层面上的表现形式：table表目录下以子文件夹形式存在。

- 一个文件夹表示一个分区。分区命名标准：分区列=分区值。

- 支持多重分区。

**查看所有分区：**

show partitions 表名

**增加分区：**

~~~sql
alter table 表名 
add partitions(day='20220403');
~~~

在元数据中增加分区信息，HDFS中创建分区路径。

增加多个分区时，中间不能有逗号

删除（drop）多个分区时，中间必须有逗号



**修复分区：**

msck repair table 表名 add/drop/sync

sync:同步元数据分区信息和HDFS路径

默认add



#### 分桶

优化手段表。根据表中字段的值，经过hash计算将数据文件划分成指定的若干个小文件。



### Hive和MySQL对比

Hive只适合用来做 海量数据的离线分析。Hive的定位是数据仓库，面向分析的OLAP系统。

|              | Hive           | MySQL            |
| ------------ | -------------- | ---------------- |
| 定位         | 数据仓库       | 数据库           |
| 使用场景     | 离线数据分析   | 业务数据事务处理 |
| 查询语言     | HQL            | SQL              |
| 数据存储     | HDFS           | Local FS         |
| 执行引擎     | MR、Tez、Spark | Exutor           |
| 执行延迟     | 高             | 低               |
| 处理数据规模 | 大             | 小               |
| 常见操作     | 导入数据、查询 | 增删改查         |



## Hive安装部署



### Hive服务部署





### 小结

**非交互式模式**：适用于每天固定执行的操作

无需进入hive交互页面

hive -f hello.sql

-f参数执行文件中的sql语句

hive -e "sql语句"

-e参数直接执行，执行完毕自动退出



## DDL

### 表table

#### 创建表

##### 

##### 普通表

~~~SQL
CREATE [TEMPORARY] [EXTERNAL] TABLE [IF NOT EXISTS] [db_name.]table_name   
[(col_name data_type [COMMENT col_comment], ...)]
[COMMENT table_comment]
[PARTITIONED BY (col_name data_type [COMMENT col_comment], ...)]
[CLUSTERED BY (col_name, col_name, ...) 
[SORTED BY (col_name [ASC|DESC], ...)] INTO num_buckets BUCKETS]
[ROW FORMAT row_format] 
[STORED AS file_format]
[LOCATION hdfs_path]
[TBLPROPERTIES (property_name=property_value, ...)]
~~~



**（1）TEMPORARY**
临时表，该表只在当前会话可见，会话结束，表会被删除。
**（2）EXTERNAL（重点）**
外部表，与之相对应的是内部表（管理表）。管理表意味着Hive会完全接管该表，包括元数据和HDFS中的数据。而外部表则意味着Hive只接管元数据，而不完全接管HDFS中的数据。
**（3）data_type（重点）**
Hive中的字段类型可分为基本数据类型和复杂数据类型

**基本数据类型：**

| Hive      | 说明                                                | 定义          |
| --------- | --------------------------------------------------- | ------------- |
| tinyint   | 1byte有符号整数                                     |               |
| smallint  | 2byte有符号整数                                     |               |
| int       | 4byte有符号整数                                     |               |
| bigint    | 8byte有符号整数                                     |               |
| boolean   | 布尔类型，true或者false                             |               |
| float     | 单精度浮点数                                        |               |
| double    | 双精度浮点数                                        |               |
| decimal   | 十进制精准数字类型                                  | decimal(16,2) |
| varchar   | 字符序列，需指定最大长度，最大长度的范围是[1,65535] | varchar(32)   |
| string    | 字符串，无需指定最大长度                            |               |
| timestamp | 时间类型                                            |               |
| binary    | 二进制数据                                          |               |

**复杂数据类型：**

| 类型   | 说明                                                     | 定义                        | 取值       |
| ------ | -------------------------------------------------------- | --------------------------- | ---------- |
| array  | 数组是一组相同类型的值的集合                             | array<string>               | arr[0]     |
| map    | map是一组相同类型的键-值对集合                           | map<string, int>            | map['key'] |
| struct | 结构体由多个属性组成，每个属性都有自己的属性名和数据类型 | struct<id:int, name:string> | struct.id  |

Hive的基本数据类型可以做类型转换，转换的方式包括隐式转换以及显示转换。
**方式一：隐式转换**
具体规则如下：
①任何整数类型都可以隐式地转换为一个范围更广的类型，如tinyint可以转换成int，int可以转换成bigint。
②所有整数类型、float和string类型都可以隐式地转换成double。
③tinyint、smallint、int都可以转换为float。
④boolean类型不可以转换为任何其它的类型。
详情可参考[Hive官方文档说明](https://cwiki.apache.org/confluence/display/hive/languagemanual+types#LanguageManualTypes-AllowedImplicitConversions)

**方式二：显示转换**
可以借助cast函数完成显示的类型转换





**（4）PARTITIONED BY（重点）**
创建分区表。
**（5）CLUSTERED BY ... SORTED BY...INTO ... BUCKETS（重点）**
创建分桶表。

**（6）ROW FORMAT（重点）**
指定SERDE，SERDE是Serializer and Deserializer的简写。Hive使用SERDE序列化和反序列化每行数据。详情可参考 Hive-Serde。语法说明如下：
语法一：DELIMITED关键字表示对文件中的每个字段按照特定分割符进行分割，其会使用默认的SERDE对每行数据进行序列化和反序列化。

```SQL
ROW FORAMT DELIMITED 
[FIELDS TERMINATED BY char] 
[COLLECTION ITEMS TERMINATED BY char] 
[MAP KEYS TERMINATED BY char] 
[LINES TERMINATED BY char] 
[NULL DEFINED AS char]
```

注：

```
fields terminated by ：列分隔符。
collection items terminated by ： map、struct和array中每个元素之间的分隔符。
map keys terminated by ：map中的key与value的分隔符。
lines terminated by ：行分隔符。
```

语法二：SERDE关键字可用于指定其他内置的SERDE或者用户自定义的SERDE。例如JSON SERDE，可用于处理JSON字符串。

```SQL
ROW FORMAT SERDE serde_name [WITH SERDEPROPERTIES (property_name=property_value,property_name=property_value, ...)]
```



**（7）STORED AS（重点）**
指定文件格式，常用的文件格式有，textfile（默认值），sequence file，orc file、parquet file等。
**（8）LOCATION**
指定表所对应的HDFS路径，若不指定路径，其默认值为

（不影响已存在的数据）

```sql
${hive.metastore.warehouse.dir}/db_name.db/table_name
```



**（9）TBLPROPERTIES**
用于配置表的一些KV键值对参数。

 **(10) COMMENT 表注释**





##### 复杂表

如下JSON文件由Hive进行分析处理

```
{
    "name": "dasongsong",
    "friends": [
        "bingbing",
        "lili"
    ],
    "students": {
        "xiaohaihai": 18,
        "xiaoyangyang": 16
    },
    "address": {
        "street": "hui long guan",
        "city": "beijing",
        "postal_code": 10010
    }
}
```



我们可以考虑使用专门负责**JSON**文件的**JSON Serde**，设计表字段时，表的字段与JSON字符串中的一级字段保持一致，对于具有嵌套结构的JSON字符串，考虑使用合适复杂数据类型保存其内容。最终设计出的表结构如下：

```sql
hive>
create table teacher
(
    name     string,
    friends  array<string>,
    students map<string,int>,
    address  struct<city:string,street:string,postal_code:int>
)
row format serde 'org.apache.hadoop.hive.serde2.JsonSerDe'
location '/user/hive/warehouse/teacher';
```





##### 特殊建表

**将查询到的数据建一张新表，新表中包含查询到的数据**

create table [] as select * from [];



**复刻一张表，不包含数据**

Create Table [] Like 表；





#### 查看表

**查看所有表**

~~~sql
SHOW TABLES [IN database_name] LIKE
~~~

**查看表信息**

~~~sql
DESCRIBE [EXTENDED | FORMATTED] [db_name.]table_name
~~~

extened：详细信息

formatted：对详细信息格式化展示



#### 修改表

**重命名**

~~~sql
ALTER TABLE table_name RENAME TO new_table_name
~~~



**增加列**

~~~sql
ALTER TABLE table_name ADD COLUMNS (col_name data_type [COMMENT col_comment], ...)
~~~



**更新列**

~~~sql
--该语句允许用户修改指定列的列名、数据类型、注释信息以及在表中的位置。
ALTER TABLE table_name CHANGE [COLUMN] col_old_name col_new_name column_type [COMMENT col_comment] [FIRST|AFTER column_name]
~~~

**替换列**

~~~sql
--该语句允许用户用新的列集替换表中原有的全部列。
ALTER TABLE table_name REPLACE COLUMNS (col_name data_type [COMMENT col_comment], ...)
~~~



#### 删除表

~~~sql
DROP TABLE [IF EXISTS] table_name;
~~~



#### 清空表

~~~sql
TRUNCATE [TABLE] table_name
~~~

只对内部表起作用



### DML

#### Load

~~~sql
LOAD DATA [LOCAL] INPATH 'filepath' [OVERWRITE] INTO TABLE tablename；
~~~

**关键字说明：**

（1）local：表示从本地加载数据到Hive表；否则从HDFS加载数据到Hive表。

（2）overwrite：表示覆盖表中已有数据，否则表示追加。

（3）partition：表示上传到指定分区，若目标是分区表，需指定分区。



#### Insert

将查询结果插入到另一张表中

~~~sql
INSERT (INTO | OVERWRITE) TABLE tablename [PARTITION (partcol1=val1, partcol2=val2 ...)] select_statement;
~~~

（1）INTO：将结果追加到目标表

（2）OVERWRITE：用结果覆盖原有数据

（3）PARITIION：分区

~~~sql
insert overwrite table student3
select 
    id, 
    name 
from student;
~~~

将给定的values插入表中

~~~sql
insert into table  student1 values(1,'wangwu'),(2,'zhaoliu');
~~~

将查询结果写入目标路径

~~~sql
INSERT OVERWRITE [LOCAL] DIRECTORY --路径
  [ROW FORMAT row_format]
  [STORED AS file_format] --存储格式
  select_statement;
~~~



#### Export&Import

Export导出语句可将表的数据和元数据信息一并到处的HDFS路径，Import可将Export导出的内容导入Hive，表的数据和元数据信息都会恢复。Export和Import可用于两个Hive实例之间的数据迁移。

**语法**

~~~sql
--导出
EXPORT TABLE tablename TO 'export_target_path'

--导入
IMPORT [EXTERNAL] TABLE new_or_original_tablename FROM 'source_path' [LOCATION 'import_target_path']
~~~

EXTERNAL：外部表

LOCATION：修改hive表元数据默认存储位置（不影响已存在的数据）

### 查询

#### 基本查询

**order by** 

排序

asc升序 desc降序

只能有一个**reduce**才能实现全部有序

order by 和 limit经常组合使用，reduce在map端只拿到指定数量的数据



**sort by** 每个reduce内部有序

不只有一个reduce，并非全局有序

查询结果输出到本地产生不止一个文件



**distribute by**

分类放入不同的reduce

例如将相同部门的输出到一个最终文件里

distribute by deptno





#### Join

**默认内连接**

join

**左外连接**

lift join

**右外连接**

right join

**满外连接**

full join

#### union

连接两个查询结果（两个select）

列名可以不一致，最终查询结果的字段名为第一个表的字段名。

union去重 union all不去重

~~~sql
select *
from (
  select_statement
  union [all | distinct]
  select_statement
) union_result
~~~





### 函数



#### 数值函数

round 四舍五入取整

语法：round(double A)或round(double A,int b)。

说明：round函数只传入一个double类型的参数时，会遵循四舍五入原则返回double类型数值的整数部分。当传入第二个整数类型的参数时，会遵循四舍五入原则返回指定精度的double类型的结果。



ceil： 向上取整

floor： 向下取整

rand：随机数函数

abs：绝对值函数



#### 字符串函数

**substring ：截取字符串**

语法一：substring(string A, int start)  返回值：string

返回字符串A从start位置到结尾的字符串

语法二：substring(string A, int start, int len) 

说明：返回字符串A从start位置开始，长度为len的字符串。



**replace：替换**

语法：replace(string A, string B, string C) 

返回值：string

说明：将字符串A中的子字符串B替换为C。



regexp_replace：正则替换

语法：regexp_replace(string A, string B, string C) 

返回值：string

说明：将字符串A中的符合Java正则表达式pattern的部分替换为replacement。注意，在有些情况下要使用转义字符。

![image-20230821134035287](Hive.assets/image-20230821134035287.png)



**regexp：正则匹配**

语法：字符串 regexp 正则表达式
返回值：boolean
说明：若字符串符合正则表达式，则返回true，否则返回false

~~~sql
-->select 'dfsaaaa' regexp 'dfsa+'
-->true
~~~

**repeat：重复字符串**

语法：repeat(string A, int n)

返回值：string

说明：将字符串A重复n遍，组成一个新的字符串。

![image-20230821134311166](Hive.assets/image-20230821134311166.png)

**split：字符串切割**

语法：split(string str, string pat) 

返回值：array

说明：按照正则表达式pat的内容切割字符串str，切割后的字符串，以数组的形式返回。需要注意的是，如果选择的分隔符在正则表达式中有特殊含义，则需要对分隔符进行转义。例如，对字符串“192.168.11.12”按照“.”进行切割，则函数使用方式为split('192.168.11.12', '\\.')。

![image-20230821134403157](Hive.assets/image-20230821134403157.png)

**concat ：拼接字符串**

语法：concat(string A, string B, string C, ……) 

返回：string

说明：将A,B,C……等字符拼接为一个字符串

![image-20230821134655059](Hive.assets/image-20230821134655059.png)

**concat_ws：以指定分隔符拼接字符串或者字符串数组**

语法：concat_ws(string A, string…| array(string)) 

返回值：string

说明：使用分隔符A拼接多个字符串，或者一个数组的所有元素。

![image-20230821135326462](Hive.assets/image-20230821135326462.png)



**nvl**

nvl(字段名，值) 如果字段为空，则赋值为第二个参数



**get_json_object：解析json字符串**

语法：get_json_object(string json_string, string path) 

返回值：string

说明：解析json的字符串json_string，返回path指定的内容。如果输入的json字符串无效，那么返回NULL。



#### 日期函数

**unix_timestamp()**

语法一：unix_timestamp()。

说明：返回当前时间的UNIX时间戳，这个用法已经被弃用，改用current_timestamp函数获取当前时间戳。

语法二：unix_timestamp(string date)。

说明：返回指定时间date的UNIX时间戳，时间date需要是yyyy-MM-dd HH:mm:ss格式，如果转换失败，则返回0。

语法三：unix_timestamp(string date, string pattern)。

说明：转换指定的pattern格式的日期为UNIX时间戳。

~~~sql
hive> select unix_timestamp('2022/08/08 08-08-08','yyyy/MM/dd HH-mm-ss');  

-->1659946088
~~~



**from_unixtime**

将时间戳转换为时间

form_utc_timestamp()

转换时更改时区，整数单位为毫秒



**current_date**
说明：返回当前的日期，格式为yyyy-MM-dd



 **current_timestamp**：当前的日期加时间，并且精确的毫秒 

为当前时区时间

**datediff**：两个日期相差的天数（结束日期减去开始日期的天数）

语法：datediff(string enddate, string startdate) 。
返回值：int。
说明：计算两个日期相差的天数（enddate减去startdate的天数）。

![image-20230821145854217](Hive.assets/image-20230821145854217.png)

**date_add：日期加天数**

语法：date_add(string startdate, int days) 

返回值：string 

说明：返回开始日期 startdate 增加 days 天后的日期。

![image-20230821150003266](Hive.assets/image-20230821150003266.png)

**date_sub：日期减天数**

**date_format:将标准日期解析成指定格式字符串**

语法：date_sub (date/timestamp/string ts, string fmt)

返回值：string。

说明：将标准日期、时间戳或日期字符串解析成指定格式的字符串，日期和时间格式由日期和时间模式字符串fmt指定。常用的模式字母如下。

-  y，代表年份，yyyy会将日期格式化成四位数年份，yy会将日期格式化成年份后两位。

- M，代表月份，MMM会将日期中的月份格式化成文本，如Jul、Jan；MM会将日期中的月份格式化成数字，如07、01。

- ld，代表月份中的天数，使用方式为dd。

- u，星期几的天数（1代表星期一，2代表星期二，以此类推）。

-  H，一天中的小时数，使用方式为HH。

-  m，小时中的分钟数，使用方式为mm。

- s，分钟中的秒数，使用方式为ss。

![image-20230821150310623](Hive.assets/image-20230821150310623.png)



#### 流程控制函数

**nvl：控制查找函数**

语法：nvl(A,B)。 

说明：若A的值不为null，则返回A，否则返回B。

![image-20230821150540712](Hive.assets/image-20230821150540712.png)



**case when：条件判断函数**

~~~sql
select
case score
when 1 then a
when 2 then b
else c
~~~





**if: 条件判断，类似于Java中三元运算符**

~~~sql
select if(10>5,'正确','错误')；
~~~



#### 集合函数

处理复杂数据类型

##### array

**select array('','','')**

返回数组

**array_contains: 判断array中是否包含某个元素**

语法：array_contains(Array<T>, value)。

返回值：boolean。

说明: 判断array中是否包含value元素，如果存在返回true

![image-20230821152232168](Hive.assets/image-20230821152232168.png)

**sort_array：将array中的元素排序**

只能升序

**size：集合中元素的个数**

返回值：int



##### map

**map：创建map集合**

语法：map (key1, value1, key2, value2, …) 

说明：根据输入的key和value对构建map类型。

**map_keys： 返回map中的key**
语法：map_keys(map<K.V>)。
返回值：array<K>。



##### struct

**named_struct声明struct的属性和值**

~~~
select named_struct('字段名','值','字段名','值')
~~~



#### 高级聚合函数

**collect_list 形成list集合，不去重**

~~~sql
--查询每个月的入职人数以及姓名
select
	substring(hiredate,1,7)
	count(*)
	collect_list(name)
from employee
group by substring(hiredate,1,7)
~~~



**collect_set 形成set集合，去重**



#### 炸裂函数UDTF

**explode(array m)**

将数组拆分成多行

**explode(map<k,v> m)**



**posexplode(array<T> a)**

返回索引和...

**inline(array<struct<>> a)**

一个结构体一行，字段名为结构体字段名



**lateral view**

通常与UDTF配合使用。lateral view可以将UDTF应用到原表的每行数据，将每行数据转换为一行或多行，并将源表中每行的输出结果与该行连接起来，形成一个虚拟表。

~~~
select 
	col1 [,col2,col3……] 
from 表名 
lateral view expolde(拆分字段) 虚拟表别名 as col1 [,col2,col3……]
~~~



#### 窗口函数

**基本语法：**

~~~sql
select 
    col_1, 
    col_2, 
    col_3, 
    函数(col_1) over (窗口范围) as 别名
from table_name;
~~~

**基于行**的窗口范围定义语法如下。

~~~sql
sum(amount) over(order by <column> rows between <start> and <end>)
~~~

order by 排序

如果不进行排序，程序运行时，**reduce阶段**的顺序和表中可见的顺序不一定相同。

start 起点  and 终点



![image-20230822133312050](Hive.assets/image-20230822133312050.png)



**基于值**的窗口范围定义语法如下，与基于行的窗口范围定义不同的是，使用的是**range**关键字，而不是**rows** 。

sum(amount) over(order by <column> range between <start> and <end>)

这里order by 排序无意义，作用是指定基于哪个字段的值进行窗口划分

![image-20230822141308172](Hive.assets/image-20230822141308172.png)

**分区：**

定义窗口范围时，我们还可以使用**partition by**关键字指定分区列，将每个分区单独划分为窗口。

partition by放在order by前

作用：比如区分不同商品或者用户

**缺省：**

order by 省略，rows不排序



between...and...省略不写

over()中包含order by 默认值为：

range between unbounded preceded and current row 负无穷到当前值

over()中不包含order by 默认值为：

rows 第一行到当前行



##### 常用窗口函数

**聚合函数：**
max：最大值。
min：最小值。
sum：求和。
avg：平均值。
count：计数。

**跨行取值函数：**

**lead和lag**

lead函数：用于获取窗口内当前行往下第n行的值。

~~~sql
select
	col1，
	col2,
	lag(),
	lead()
form 表
~~~



语法：lead(col, n, default)

说明：第一个参数为列名，第二参数为往下第几行，第三个参数为当往下第几行遇到null值时，所取的默认值。第二个参数可选，其默认值为1，第三个参数的默认值为null。

lag：往上

lag和lead函数不支持使用rows between和range between的自定义窗口。



![image-20230822142534115](Hive.assets/image-20230822142534115.png)

betweeb...and...默认值？

负无穷到当前值



**排序函数：**

排名函数是窗口函数的使用中非常频繁的一种，主要包括rank 、dense_rank、row_number

~~~sql
rank()/dense_rank()/row_number() over (partition by col1 order by col2)
~~~



排名函数会对窗口范围内的数据按照order by后的列进行排名。如上所示对排名函数的使用，会先根据col1列对数据进行分区，在分区内根据col2列进行升序或降序排序，然后生成一列新的排序序号列，生成序号的规则区别如下。

- lrank函数，生成的序号从1开始，若col2列的值相同，则排序序号相同，且会在序号中留下空位。

- ldense_rank函数，生成的序号从1开始，若col2列的值相同，则排序序号相同，单不会在序号中留下空位。

- lrow_number函数，生成的序号从1开始，按照顺序生成序号，不会存在相同的序号。

![image-20230822144059326](Hive.assets/image-20230822144059326.png)



### 其他

**函数复用**

CTE复用：with t1 as();

如果with子句在后面要多次使用到，这可以大大的简化SQL

~~~
WITH t1 AS (
		SELECT *
		FROM carinfo
	), 
	t2 AS (
		SELECT *
		FROM car_blacklist
	)
SELECT *
FROM t1, t2
~~~

**注意：这里必须要整体作为一条sql查询，即with as语句后不能加分号，不然会报错。**
