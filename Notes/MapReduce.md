

# MapReduce

### 概述

**MapReduce: 自己处理业务相关代码+ 自身默认的代码。**

#### 优点：

1. 易于编程。用户只关心业务逻辑。实现框架的接口。
2. 良好的扩展性：可以动态的增加服务器，解决计算资源不足的问题。
3. 高容错性：任何一台机器垮掉，可以将任务转移到其他节点。
4. 适合海量数据计算（TB/PB），几千台服务器共同计算

#### 缺点：

1. 不擅长实时计算。
2. 不擅长流式计算。Sparkstreaming Filnk擅长
3. 不擅长DAG有向无环图计算。（任务一的结果作为任务二的输入）。spark擅长

#### MapReduce进程

一个完整的MapReduce程序在分布式运行时有三类实例进程

1. MrAppMaster: 负责整个程序的过程及状态协调。
2. MapTask：负责Map的整个数据处理流程。
3. ReduceTask：负责Reduce阶段的整个数据处理流程。





#### MapReduce核心思想

![image-20230803160357727](MapReduce.assets/image-20230803160357727.png)

（1）分布式的运算程序往往需要分成至少2个阶段。

（2）第一个阶段的MapTask并发实例，完全并行运行，互不相干。

（3）第二个阶段的ReduceTask并发实例互不相干，但是他们的数据依赖于上一个阶段的所有MapTask并发实例的输出。

（4）MapReduce编程模型只能包含一个Map阶段和一个Reduce阶段，如果用户的业务逻辑非常复杂，那就只能多个MapReduce程序，串行运行。

总结：分析WordCount数据流走向深入理解MapReduce核心思想。



#### 常用数据序列化类型

| **Java类型** | **Hadoop Writable类型** |
| ------------ | ----------------------- |
| Boolean      | BooleanWritable         |
| Byte         | ByteWritable            |
| Int          | IntWritable             |
| Float        | FloatWritable           |
| Long         | LongWritable            |
| Double       | DoubleWritable          |
| String       | Text                    |
| Map          | MapWritable             |
| Array        | ArrayWritable           |
| Null         | NullWritable            |



#### 编程规范

用户编写的程序分成三个部分：Mapper、Reducer和Driver。

![image-20230803160614830](MapReduce.assets/image-20230803160614830.png)

![image-20230803160643246](MapReduce.assets/image-20230803160643246.png)



### 序列化概述

#### 序列化定义

- **序列化**就是把内存中的对象转**换成字节序列** (或者其他数据传输协议) 以便于储存到磁盘 (持久化) 和网络传输。
- **反序列化**就是将接收到的字节序列 (或其他数据传输协议) 或者是磁盘持久化数据，**转换成为内存中的对象。**

#### 序列化的应用

对象只存在于内存中，关机断电就没有了，而且激活的对象只能由本地的进程使用，不能被发送到网络上的另一台计算机上。序列化是程序数据存储的一种形式。储存的数据可以被再次提取以及发送到另一台设备上。

#### Hadoop序列化

Java的序列化是一个重量级的序列化框架 (Serializable),一个对象被序列化后，会附带很多额外的信息(各种校验信息，Header，继承体系等)，不便于在网络中高效的传输。**所以，Hadoop自己开发了一套序列化机制(Writable)**。

#### Hadoop序列化的特点

1. 紧凑: 高效使用储存空间。
2. 快速：读写数据的额外开销小。
3. 可扩展：随着通信协议的升级而可以升级。
4. 互操作：支持多种语言交互。

#### Hadoop 自定义对象序列化

在企业开发中往往常用的基本序列化类型不能满足所有需求，比如在Hadoop框架内部传递一个bean对象，那么该对象就需要实现序列化接口。 具体实现bean对象序列化步骤如下7步。 

1. 必须实现Writable接口 
2.  反序列化时，需要反射调用空参构造函数，所以必须有空参构造

```java
public Bean() {
    super();
}
```

   3.重写序列化方法

```java
@Override
public void write(DataOutput out) throws IOException {
    out.writeLong(attr1);
    out.writeLong(attr2);
    out.writeLong(attr3);
}
```

   4.重写反序列化方法

```java
@Override
public void readFields(DataInput in) throws IOException {
    attr1 = in.readLong();
    attr2 = in.readLong();
    attr3 = in.readLong();
}
```

   5.注意反序列化的顺序和序列化的顺序完全一致

   6.要想把结果显示在文件中，需要重写toString()，可用"\t"分开，方便后续用。

   7.如果需要将自定义的bean放在key中传输，则还需要实现Comparable接口，因为MapReduce框中的Shuffle过程要求对key必须能排序。详见后面排序案例。

```
@Override
public int compareTo(FlowBean o) {
	// 倒序排列，从大到小
	return this.sumFlow > o.getSumFlow() ? -1 : 1;
}
```



### MapReduce框架原理



#### IntputFormat数据输入



##### 切片与MapTsak并行度决定机制

MapTsak个数，决定并行度。相互之间不交互，并行度决定Map阶段的任务处理并发度，进而影响到整个Job的处理速度。



**数据块：**Block是HDFS物理上把数据分成一块一块。数据块是HDFS存储数据单位。

**数据切片：**数据切片只是在逻辑上对输入进行分片，并不会在磁盘上将其切分成片进行存储。数据切片是MapReduce程序计算输入数据的单位，一个切片会对应启动一个MapTask。



![image-20230806175102217](MapReduce.assets/image-20230806175102217.png)



##### Job

##### FileInputFormat切片机制

**机制：**

- 简单地按照文件的长度进行切片
- 切片大小默认等于Block大小
- 切片时不考虑数据集整体，而是针对每一个文件单独切片



##### TextInputFormat



##### CombineTextInputFormat切片机制

框架默认的TextInputFormat切片机制是对任务按文件规划切片，不管文件多小，都会是一个单独的切片，都会交给一个MapTask，这样如果有大量小文件，就会产生大量的MapTask，处理效率极其低下。

**应用场景:**

CombineTextInputFormat用于小文件过多的场景，它可以将多个小文件从逻辑上规划到一个切片中，这样，多个小文件就可以交给一个MapTask处理。

**虚拟存储切片最大值设置:**

CombineTextInputFormat.setMaxInputSplitSize(job, 4194304);// 4m

注意：虚拟存储切片最大值设置最好根据实际的小文件大小情况来设置具体的值。

**切片机制:**

生成切片过程包括：虚拟存储过程和切片过程二部分。

![image-20230806183609735](MapReduce.assets/image-20230806183609735.png)

**驱动类添加：**

~~~Java
// 如果不设置InputFormat，它默认用的是TextInputFormat.class

job.setInputFormatClass(CombineTextInputFormat.class);

//虚拟存储切片最大值设置4m

CombineTextInputFormat.setMaxInputSplitSize(job, 4194304);
~~~

运行如果为3个切片：

number of splits:3



#### MapReduce工作流程

![image-20230806205753408](MapReduce.assets/image-20230806205753408.png)



![image-20230806205850080](MapReduce.assets/image-20230806205850080.png)





##### MapTask：

分区计算之后，将数据写入内存，环形缓冲区，减少磁盘IO次数

缓冲区默认大小100MB。

溢写比例spill.percent：0.8



当缓冲区数据达到阈值80，溢写线程启动，锁定这80MB内存，执行溢写过程。MapTask的输出结果还可以往剩下的20MB内存中写，互不影响。



溢写线程启动后，需要对这80MB空间内的Key做排序sort。

如果Job设置过combiner，那么就是使用combiner的时候，将有相同key的key/value相加，减少溢写到磁盘的数据量。



merge合并



分区与ReduceTask个数有关

 



##### ReduceTask:

1. copy阶段，简单的拉取数据。启动一些数据copy线程Fetcher，HTTP方式请求maptask获取属于自己的文件。

2. merge阶段，copy过来的数据会先放入内存缓冲区，这里缓冲区大小比map端的更为灵活。

   merge有三种形式：内存到内存；内存到磁盘；磁盘到磁盘。默认情况第一种形式不启用。当内存中的数据量达到一定阈值，就启动内存到磁盘的merge。第二种merge方式一直在运行，直到没有map端的数据才结束，然后启动第三种merge方式生成最终的文件。

3. merge后进行sort。默认key的字典序排序。

4. 对排序后的键值对调用reduce方法，键相等的键值对组成一组，调用一次reduce方法。

   reduce处理结果会调用默认输出组件TextOutputFormat写入到指定目录中，默认一次写一行。



**并行度决定机制：**

![image-20230808123942773](MapReduce.assets/image-20230808123942773.png)



#### Shuffle

![image-20230806210107838](MapReduce.assets/image-20230806210107838.png)



shuffle本意是“洗牌”

更像是洗牌的逆过程。

shuffle是MapReduce的核心，分布在map和reduce阶段。

一般把从map产生输出开始到reduce取得数据作为输入之前的过程称为shuffle。



Map端Shuffle

- Collect阶段：将MapTask的结果输出到默认大小为100的环形缓冲区，保存之前对key进行分区的计算，默认Hash分区。
- Spill阶段：当内存中的数据达到一定的阈值之后，就会将数据写入本地磁盘，在将数据写入磁盘之前需要对数据进行一次sort操作（快排），如果配置了combiner，还会将有相同分区号和key的数据进行combiner。
- merge阶段：把所有溢出的临时文件进行一次merge操作（归并），以确保一个MapTask最终只产生一个中间数据文件。

Reduce端shuffle

- copy阶段：ReduceTask启动Fetcher线程到已经完成MapTask的节点上复制一份属于自己的数据。

- merge阶段：在ReduceTask远程复制数据的同时，会在后台开两个线程对内存到本地的数据文件进行合并操作。

  （这里应该是合并的不同MapTesk（MapTesk1、2）的数据文件）

- sort阶段：在对数据进行合并的同时，会进行排序操作（归并），由于MapTask阶段已经对数据进行了局部的排序，ReduceTask只需保证copy的数据的最终整体有效性即可。然后还要进行相同的key进行combiner。



##### Partition分区

自定义分区设置分区类，比如将手机号按照开头分区。

自定义分区是根据key的hashcode值对ReduceTask个数取模得到的。

![image-20230807182906434](MapReduce.assets/image-20230807182906434.png)

![image-20230807182931624](MapReduce.assets/image-20230807182931624.png)



##### 排序

MapTask阶段进行两次排序

在环形缓冲区溢写之前对数据进行一次快排，然后对多次产生的溢写文件进行一次归并。对key的索引按照字典排序。

![image-20230807183655283](MapReduce.assets/image-20230807183655283.png)

ReduceTask阶段

拉取自己分区的数据再进行归并排序（不同MapTask的数据）。



##### combiner

如果没有Reduce阶段，就没有Suffle阶段，combiner就不会起效果。



#### OutputFormat

防止丢数据：在rdeuce读数据阶段进行一次遍历。

![image-20230807230422414](MapReduce.assets/image-20230807230422414.png)

![image-20230808123712839](MapReduce.assets/image-20230808123712839.png)



#### Join应用

**获取文件名**

(FileSplit)(context.getInputSplit());

~~~Java
String filename = ((FileSplit)context.getInputSplit()).getPath().getName();
~~~



#### MapReduce DB操作



#### MapReduce分组



#### 排序







#### 计数器

**使用方法：**

通过context.getCounter方法获取一个全局计数器，创建的时候需要指定计数器所属的组名和计数器的名字。

1.在map方法中

~~~java
Counter counter = context.getCounter("组名","计数器名");
~~~

2.在需要使用计数器的地方增加计数器方法

官方文档中有详细方法说明

### 压缩

#### 概述

**压缩的好处和坏处**

压缩的优点：以减少磁盘IO、减少磁盘存储空间。

压缩的缺点：增加CPU开销。

**压缩原则**

（1）运算密集型的Job，少用压缩

（2）IO密集型的Job，多用压缩



#### MR支持的压缩编码

**压缩算法对比介绍**

| 压缩格式 | Hadoop自带？ | 算法    | 文件扩展名 | 是否可切片 | 换成压缩格式后，原来的程序是否需要修改 | 压缩率 | 速度 |
| -------- | ------------ | ------- | ---------- | ---------- | -------------------------------------- | ------ | ---- |
| DEFLATE  | 是，直接使用 | DEFLATE | .deflate   | 否         | 和文本处理一样，不需要修改             |        |      |
| Gzip     | 是，直接使用 | DEFLATE | .gz        | 否         | 和文本处理一样，不需要修改             | 高     | 一般 |
| bzip2    | 是，直接使用 | bzip2   | .bz2       | 是         | 和文本处理一样，不需要修改             | 高     | 慢   |
| LZO      | 否，需要安装 | LZO     | .lzo       | 是         | 需要建索引，还需要指定输入格式         | 一半   | 快   |
| Snappy   | 是，直接使用 | Snappy  | .snappy    | 否         | 和文本处理一样，不需要修改             | 一般   | 快   |



#### 压缩位置

![image-20230808143203603](MapReduce.assets/image-20230808143203603.png)



#### 实例：

在驱动类添加以下代码

**Map输出端压缩**

```
		// 开启map端输出压缩

		conf.setBoolean("mapreduce.map.output.compress", true);


		// 设置map端输出压缩方式

		conf.setClass("mapreduce.map.output.compress.codec", BZip2Codec.class,CompressionCodec.class);
```



**Reduce输出端压缩**

```
		// 设置reduce端输出压缩开启
		FileOutputFormat.setCompressOutput(job, true);

		// 设置压缩的方式
	    FileOutputFormat.setOutputCompressorClass(job, BZip2Codec.class); 
	    
//	    FileOutputFormat.setOutputCompressorClass(job, GzipCodec.class); 

//	    FileOutputFormat.setOutputCompressorClass(job, DefaultCodec.class);
```



