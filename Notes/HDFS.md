## HDFS



### 概述



**产生**：为了解决海量数据的存储问题

**定义**：

分布式文件系统。用于存储文件，通过目录树来定位文件，由很多台服务器联合起来实现其功能，集群中的服务器各有各的角色。

**使用场景**

适合一次写入，多次读出的场景，一个文件经过创建、写入和关闭后就不需要改变。

#### HDFS优缺点

**HDFS优点**

- 高容错性
  - 数据自动保存多个副本。提高容错
  - 某一个副本丢失后可以自动恢复
- 适合处理大数据
  - 数据规模：GB、TB甚至PB级别的数据；
  - 文件规模：能够处理百万规模以上的文件数量。
- 可构建在廉价机器上，通过多副本机制提高可靠性。



**HDFS缺点**

- 不适合低延时数据访问
- 无法高效的对大量小文件的访问
  - 存储大量小文件，会占用NameNode大量的内存来存储文件目录和块信息；
  - 小文件存储的寻址时间会超过读取时间，违法了HDFS的设计目标。
- 不支持并发写入、文件随机修改。
  - 一个文件只能有一个写，不允许多个线程同时写入；
  - 仅支持数据追加（append），不支持随机修改。



#### HDFS组成架构

**NameNode(nn)：**就是Master，主管、管理者。

- 管理HDFS的名称空间；
- 设置副本策略；
- 管理数据块（Block）的映射信息；
- 处理客户端读写请求。

**DataNode：**就是Slave。NameNode下达命令，DataNode执行实际的操作。

- 实际存储的数据块；
- 执行数据块的读写操作。

**Client：**客户端

- 文件切分。文件上传HDFS的时候，Client将文件切分成一个一个的Block，然后进行上传；
- 与NameNode交互，获取文件的位置信息；
- 与DataNode交互，读取或写入数据；
- Client提供一些命令来管理HDFS，比如NameNode格式化；
- Client可以通过一些命令来访问HDFS，比如对HDFS增删查改操作。

**Secondary NameNode：**并非NameNode的热备。当NameNode挂掉的时候，它并不能马上替换NameNode并提供服务。

- 辅助NameNode，分担其工作量，比如定期合并Fsimage和Edits，并推送给NameNode；
- 在紧急情况下，可辅助恢复NameNode.



#### 文件块大小

![图片1](HDFS.assets/图片1.png)

![Snipaste_2023-07-31_17-01-49](HDFS.assets/Snipaste_2023-07-31_17-01-49.png)



-------------------------------------------



### HDFS的Shell操作



#### 基本语法

hadoop fs 具体命令  OR  hdfs dfs 具体命令

两个是完全相同的。

#### 命令大全

#### 常用命令实操

##### 准备工作

##### 上传

| 命令             | 作用                                                         |
| ---------------- | ------------------------------------------------------------ |
| -moveFromLocal： | 从本地剪切粘贴到HDFS                                         |
| -copyFromLocal： | 从本地文件系统中拷贝文件到HDFS路径去                         |
| -put：           | 等同于copyFromLocal，生产实习环境更习惯用put  例：hadoop fs -put hello.txt / |
| -appendToFile：  | 追加一个文件到已经存在的文件末尾                             |

##### 下载

| 命令         | 作用              |
| ------------ | ----------------- |
| -copyToLocal | 从HDFS拷贝到本地  |
| -get         | 等同于copyToLocal |

下载时可以修改文件名

如：hadoop fs -get /sanguo/shuguo.txt ./shuguo2.txt

##### HDFS直接操作

- -ls: 显示目录信息

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -ls /sanguo``



- -cat：显示文件内容

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -cat /sanguo/shuguo.txt``



- -chgrp、-chmod、-chown：Linux文件系统中的用法一样，修改文件所属权限

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs  -chmod 666  /sanguo/shuguo.txt``

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs  -chown  :  /sanguo/shuguo.txt``



- -mkdir：创建路径

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -mkdir /jinguo``



- -cp：从HDFS的一个路径拷贝到HDFS的另一个路径

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -cp /sanguo/shuguo.txt /jinguo``



- -mv：在HDFS目录中移动文件

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -mv /sanguo/wuguo.txt /jinguo``

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -mv /sanguo/weiguo.txt /jinguo``



- -tail：显示一个文件的末尾1kb的数据

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -tail /jinguo/shuguo.txt``



- -rm：删除文件或文件夹

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -rm /sanguo/shuguo.txt``



- -rm -r：递归删除目录及目录里面内容

``[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -rm -r /sanguo``



- -du统计文件夹的大小信息

``````
[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -du -s -h /jinguo

27  81  /jinguo
``````

``````

[sjy@hadoop102 hadoop-3.1.3]$ hadoop fs -du  -h /jinguo

14  42  /jinguo/shuguo.txt

7  21  /jinguo/weiguo.txt

6  18  /jinguo/wuguo.tx
``````

说明：27表示文件大小；81表示27*3个副本；/jinguo表示查看的目录



- -setrep：设置HDFS中文件的副本数量
- ![image-20230731181209262](HDFS.assets/image-20230731181209262.png)

这里设置的副本数只是记录在NameNode的元数据中，是否真的会有这么多副本，还得看DataNode的数量。因为目前只有3台设备，最多也就3个副本，只有节点数的增加到10台时，副本数才能达到10。



### API

### HDFS读写数据流程

#### HDFS写数据流程

##### 节点距离计算

在HDFS写数据的过程中，NameNode会选择距离待上传数据最近距离的DataNode接收数据。

节点距离：两个节点到达最近的==共同祖先==的距离总和。

##### 机架感知（副本存储节点选择）

##### 副本节点选择

![image-20230801180403089](HDFS.assets/image-20230801180403089.png)



#### HDFS读数据流程

**节点选择**：节点距离，负载均衡。

**读取方式**：串行，一块一块的读取

**流程**：

Create---请求下载文件--->NameNode--查询是否有该文件--返回目标文件的原数据----->Create-----创建流对象----读数据-----请求读取数据------->DataNode-----传输数据--->Create

![image-20230801181251030](HDFS.assets/image-20230801181251030.png)

（1）客户端通过DistributedFileSystem向NameNode请求下载文件，NameNode通过查询元数据，找到文件块所在的DataNode地址。

（2）挑选一台DataNode（就近原则，然后随机）服务器，请求读取数据。

（3）DataNode开始传输数据给客户端（从磁盘里面读取数据输入流，以Packet为单位来做校验）。

（4）客户端以Packet为单位接收，先在本地缓存，然后写入目标文件。

### NameNode和SecondaryNameNode

#### NN和2NN工作机制

 ![image-20230801183015336](HDFS.assets/image-20230801183015336.png)



增删改请求先记账到edits_inprogress_00，再加载到内存。

**流程**：

元数据需要存放在内存中。但如果只存在内存中，一旦断电，元数据丢失，整个集群就无法工作了。因此产生在磁盘中备份元数据的FsImage（镜像文件）。

引入Edits文件（只进行追加操作，效率很高）。

每当元数据有更新或者添加元数据时，修改内存中的元数据并追加到Edits中。

通过FsImage和Edits的合并，合成元数据。

如果长时间添加数据到Edits中，会导致该文件数据过大，效率降低，而且一旦断电，恢复元数据需要的时间过长。因此，需要定期进行FsImage和Edits的合并，如果这个操作由NameNode节点完成，又会效率过低。因此，引入一个新的节点SecondaryNamenode，专门用于FsImage和Edits的合并。



#### CheckPoint时间设置

**通常情况下，SecondaryNameNode每隔一小时执行一次**

​	[hdfs-default.xml]

<property>

 <name>dfs.namenode.checkpoint.period</name>

 <value>3600s</value>

</property>



**一分钟检查一次操作次数，当操作次数达到1百万时，SecondaryNameNode执行一次**

<property>

 <name>dfs.namenode.checkpoint.txns</name>

 <value>1000000</value>

<description>操作动作次数</description>

</property>

 

<property>

 <name>dfs.namenode.checkpoint.check.period</name>

 <value>60s</value>

<description> 1分钟检查一次操作次数</description>

</property>



### DataNode

#### DataNode工作机制

![image-20230801190908410](HDFS.assets/image-20230801190908410.png)

（1）一个数据块在DataNode上以文件形式存储在磁盘上，包括两个文件，一个是数据本身，一个是元数据包括数据块的长度，块数据的校验和，以及时间戳。

（2）DataNode启动后向NameNode注册，通过后，周期性（6小时）的向NameNode上报所有的块信息。



DN向NN汇报当前解读信息的时间间隔，默认6小时；

```
<property>

​	<name>dfs.blockreport.intervalMsec</name>

​	<value>21600000</value>

​	<description>Determines block reporting interval in milliseconds.</description>

</property>
```



DN扫描自己节点块信息列表的时间，默认6小时

```
<property>

​	<name>dfs.datanode.directoryscan.interval</name>

​	<value>21600s</value>

​	<description>Interval in seconds for Datanode to scan data directories and reconcile the difference between blocks in memory and on the disk.

​	Support multiple time unit suffix(case insensitive), as described

​	in dfs.heartbeat.interval.

​	</description>

</property>
```



（3）心跳是每3秒一次，心跳返回结果带有NameNode给该DataNode的命令如复制块数据到另一台机器，或删除某个数据块。如果超过10分钟没有收到某个DataNode的心跳，则认为该节点不可用。

（4）集群运行中可以安全加入和退出一些机器。

#### 数据的完整性

校验方式：CRC32



### 总结

块大小和内存读写速度有关

掉线参数可灵活调整

