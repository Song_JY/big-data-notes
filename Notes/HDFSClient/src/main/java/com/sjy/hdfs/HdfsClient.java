package com.sjy.hdfs;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

public class HdfsClient {

    private FileSystem fs;

    @Before
    public void init() throws URISyntaxException, IOException, InterruptedException {
        // 连接的集群nn地址
        URI uri = new URI("hdfs://hadoop102:8020");
        // 创建一个配置文件
        Configuration configuration = new Configuration();

        configuration.set("dfs.replication", "2");
        //用户
        String user = "sjy";


        //1.获取到客户端对象
        fs = FileSystem.get(uri, configuration, user);
    }

    @After
    public void close() throws IOException {

        //3.关闭资源
        fs.close();

    }

    //创建目录
    @Test
    public void testmkdir() throws URISyntaxException, IOException, InterruptedException {

        //2.创建一个文件夹
        fs.mkdirs(new Path("/xiyou/huaguoshan1"));
    }

    //上传

    /**
     * 参数优先级（以文件副本数为例）
     * hdfs-default.xml => hdfs-site.xml => 在项目资源目录下的配置文件 => 代码里的配置
     *
     * @throws IOException
     */

    @Test
    public void testPut() throws IOException {
        //参数解读：参数一：删除原数据；参数二：是否允许覆盖；参数三：原数据路径；参数四：目的路径。
        fs.copyFromLocalFile(false, true, new Path("D:\\sunwukong.txt"), new Path("hdfs://hadoop102/xiyou/huaguoshan"));
    }

    //文件下载
    @Test
    public void testGet() throws IOException {
        //参数解读：参数一：原文件是否删除；参数二：原文件路径；参数三：目标地址路径；参数四：是否不校验
        fs.copyToLocalFile(true, new Path("hdfs://hadoop102/xiyou/huaguoshan"), new Path("D:\\"), true);
    }

    @Test
    public void testRm() throws IOException {
        //参数解读：参数一：要删除的路径；参数二：是否递归删除
        //删除文件
        //fs.delete(new Path("/jdk-8u212-linux-x64.tar.gz"), false);

        //删除空目录
        //fs.delete(new Path("/xiyou"), false);

        //删除非空目录
        fs.delete(new Path("/jinguo"), true);
    }

    //文件的更名和移动
    @Test
    public void testmv() throws IOException {
        //参数：参数一：原文件路径；参数二：目标文件路径。
        //文件名称修改
        //fs.rename(new Path("/input/word.txt"),new Path("/input/ss.txt"));

        //文件移动和更名
        //fs.rename(new Path("/input/ss.txt"),new Path("/cls.txt"));

        //目录更名
        fs.rename(new Path("/output2"), new Path("/output3"));
    }

    @Test
    public void fileDetail() throws IOException {
        //获取所有文件信息（返回迭代器）
        RemoteIterator<LocatedFileStatus> listFiles = fs.listFiles(new Path("/"), true);

        //遍历文件
        while (listFiles.hasNext()) {
            LocatedFileStatus fileStatus = listFiles.next();

            System.out.println("=========" + fileStatus.getPath() + "=========");
            System.out.println(fileStatus.getPermission());
            System.out.println(fileStatus.getOwner());
            System.out.println(fileStatus.getGroup());
            System.out.println(fileStatus.getLen());
            System.out.println(fileStatus.getModificationTime());
            System.out.println(fileStatus.getReplication());
            System.out.println(fileStatus.getBlockSize());
            System.out.println(fileStatus.getPath().getName());

            //获取块信息
            BlockLocation[] blockLocations = fileStatus.getBlockLocations();
            System.out.println(Arrays.toString(blockLocations));
        }
    }

    //判断是文件夹还是文件
    @Test
    public void testFile() throws IOException {
        //获取哪个目录
        FileStatus[] listStatus = fs.listStatus(new Path("/"));
        //遍历文件
        for (FileStatus status : listStatus) {
        //判断
            if (status.isFile()) {
                System.out.println("文件：" + status.getPath().getName());
            } else {
                System.out.println("目录：" + status.getPath().getName());
            }
        }
    }

}
