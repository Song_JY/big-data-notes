# YARN

## Yarn资源调度器

### Yarn基础架构

![image-20230809164141768](YARN.assets/image-20230809164141768.png)

Yarn是一个资源调度平台，负责为运算程序提供服务器运算资源，相当于一个分布式的操作系统平台，而MapReduce等运算程序则相当于运行于操作系统之上的应用程序。

YARN主要由ResourceManager、NodeManager、ApplicationMaster和Container等组件构成。



ResourceManager：

一个NodeManager节点资源紧张，任务分配给其他节点。

 







### Yarn工作机制

![image-20230809163531328](YARN.assets/image-20230809163531328.png)

​	（1）MR程序提交到客户端所在的节点。

​	（2）YarnRunner向ResourceManager申请一个Application。

​	（3）RM将该应用程序的资源路径返回给YarnRunner。

​	（4）该程序将运行所需资源提交到HDFS上。

​	（5）程序资源提交完毕后，申请运行mrAppMaster。

​	（6）RM将用户的请求初始化成一个Task。

​	（7）其中一个NodeManager领取到Task任务。

​	（8）该NodeManager创建容器Container，并产生MRAppmaster。

​	（9）Container从HDFS上拷贝资源到本地。

​	（10）MRAppmaster向RM 申请运行MapTask资源。

​	（11）RM将运行MapTask任务分配给另外两个NodeManager，另两个NodeManager分别领取任务并创建容器。

​	（12）MR向两个接收到任务的NodeManager发送程序启动脚本，这两个NodeManager分别启动MapTask，MapTask对数据分区排序。

（13）MrAppMaster等待所有MapTask运行完毕后，向RM申请容器，运行ReduceTask。

​	（14）ReduceTask向MapTask获取相应分区的数据。

​	（15）程序运行完毕后，MR会向RM申请注销自己。



![image-20230809163935223](YARN.assets/image-20230809163935223.png)

![image-20230809164036247](YARN.assets/image-20230809164036247.png)



### Yarn调度器和调度算法



公平/容量调度器默认一个default，需要创建多队列。

#### 容量调度器（Capacity Scheduler）

支持多队列，可以借资源，支持多用户，优先满足先进来的任务

![image-20230808223825713](YARN.assets/image-20230808223825713.png)



#### 公平调度器

支持多队列，可以借资源，支持多用户，在队列里的任务公平享有资源

##### 缺额

 

### 常用命令

##### yarn application查看任务

**列出所有application**

yarn application -list



**根据Application状态过滤**

yarn application -list -appStates 

（所有状态：ALL、NEW、NEW_SAVING、SUBMITTED、ACCEPTED、RUNNING、FINISHED、FAILED、KILLED）



**Kill掉Application**

yarn application -kill application_1612577921195_0001



##### 查看日志

查询Application日志：yarn logs -applicationId <ApplicationId>



##### yarn node查看节点状态

列出所有节点：yarn node -list -all



##### yarn rmadmin更新配置

加载队列配置：yarn rmadmin -refreshQueues



##### yarn queue查看队列

打印队列信息：yarn queue -status <QueueName>



### Tool接口

